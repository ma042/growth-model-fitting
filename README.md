# Growth model fitting

This project provide the script to fit growth data to theoretical growth models. 
`curve_fit` from `scipy` will be used for model fitting.

## Three models are provided 
There are three models provided in the scirpt. You can also add other models you prefer. Here are these provied model.

1. Gompertz: 

```math
y = \log N_0+(\log N_{max}-\log N_0)\times \exp \left\{ -\exp\left[ \frac{\mu e}{\log N_{max}-\log N_0} (\lambda-t)+1 \right] \right\}
```

2. Three-phase: 
```math
y = \left\{
\begin{array}{lcl}
\log N_0 && {t \le \lambda}\\
\log N_0 + \mu (t - \lambda) && {\lambda < t < t_s}\\
\log N_{max} && {t \ge t_s}
\end{array} \right.
```

3. Baranyi: 
```math
y(t) = \log N_0 + \frac{\mu}{\ln(10)} A(t) - \frac{1}{\ln(10)}\ln \left(1 + \frac{\exp\left[\mu A(t)\right] - 1}{10^{(\log N_{max} - \log N_0)}}\right)
```
```math
A(t) = t + \frac{1}{\mu}\ln\left\{\exp(-\mu t) + \exp(-\mu \lambda) - \exp\left[-\mu(t + \lambda)\right] \right\}
```

In these equations, $`y`$ is the log concentration at time $`t`$; $`N_0`$ is the initial concentration (CFU/ml); $`\log N_{max}`$ is the concentration at stationary phase; $`\mu`$ is maximum growth rate (log scale for Gompertz/Three-phase model, and ln scale for Baranyi model); $`\lambda`$ is lag time; $`t_s`$ is the time to reach stationary growth phase.

## The function for model fitting 

To fit data with growth model, a function named `growth_fit` is made. 
```python
 growth_fit(model, df, x = "Time_point", y = "Concentration",ind = ["Repeat", "Strain"])
```

In this function, the data from the input DataFrame `df` will be devided into different groups according to two supplied index in `ind` (default are `Repeat` and `Strain`). The `x` and `y` of each group of data will fit the supplied model `model`. The output parameters and residual sum of squares(RSS) will be stored in a DataFrame together with the supplied index.

For example, there is a dataframe `growth_data` with following columns.

| Strain| Repeat | Time_point | Concentration |
| ----- | ------ | ---------- | ------------- |
- Strain: the strain ID 
- Repeat: the replication times 
- Time_point: The sampling time
- Concentration: The concentration at the sampling time

We want use the data from this dataframe to fit Gompertz model by using the following code:
```python
fit_Gompertz = growth_fit(model = Gompertz,df = growth_data, x = "Time_point", y = "Concentration",ind = ["Repeat", "Strain"])
```
Or in short:
```python
fit_Gompertz = growth_fit(Gompertz, growth_data)
```

Then you can find the paramter of the fitted model in the DataFrame `fit_Gompert` with the following columns. 

|Repeat | Strain | logN0 | mu | logNmax | lamb | sd logN0 | sd mu | sd logNmax | sd lamb | RSS | model |
|-------|--------|-------|----|---------|------|----------|-------|------------|---------|-----|-------|

## Raw data requirement
The raw data should be stored in a csv file with at least four columns (see the example data file). Two of these columns are used to distinguish the data group. One column is the time point (hour) and one column is the concentration (Log CFU/ml)

## Others
At the beginning of the script, you can set the data file name and direction.

You can find a example data file in this project too. 

If there are any problems with the script you can mail: xuchuan.ma@wur.nl

If you have used this project for your research, I very much appreciate that if you can thank Xuchuan Ma in the Acknowledgement. 
