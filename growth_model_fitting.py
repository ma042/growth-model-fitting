import pandas as pd
import numpy as np
import os
from scipy.optimize import curve_fit
from inspect import getfullargspec

#data file name
file_name = "example_data_file.csv"

#file direction
file_direction = "C:/Users/ma042/OneDrive - WageningenUR/Sharing_project"#use your file direction to replace the dot, such as C:/Users/data
os.chdir(file_direction)


#---------define model
#define the Gompertz model
def Gompertz(t, logN0, mu, logNmax, lamb):
    """
    The equation of Gompertz model.
    """
    return logN0 + (logNmax-logN0)* np.exp( - np.exp(mu * np.exp(1)/(logNmax-logN0) * (lamb - t) + 1 ))

#define the Three-phase model
def Three_phase(t, logN0, mu, logNmax, lamb): 
    """
    The equation of Three-phrase model.
    """
    y = np.select([t < lamb, t >= lamb], [logN0, logN0 + mu * t - mu * lamb])
    
    return np.select([y < logNmax, y >= logNmax], [y, logNmax])

#define the Baranyi model
def Baranyi(t, logN0, mu, logNmax, lamb): 
    """
    The equation of Baranyi model.
    """
    A = t + (1/mu) * np.log(np.exp(-mu*t) + np.exp(-mu*lamb) - np.exp(-mu * (t + lamb)))
    
    return  logN0 + mu/np.log(10) * A - 1/np.log(10) * np.log(1 + (np.exp(mu * A) - 1)/10**(logNmax - logN0))


#--------make the function for model fitting
def growth_fit(model, df, x = "Time_point", y = "Concentration",ind = ["Repeat", "Strain"]):
    """
    Fit the data to growth model. Data are devided by index. 
    """
    #Take the value of index
    ind_list = []
    for i in ind:
        ind_list.append(df[i].unique())
        
    #set ind as index
    df_g = df.set_index(ind)
    
    #extract the parameter of model
    para = getfullargspec(model)[0]
    #make the list of "sd" + parameter name
    para_sd = ["sd " + i for i in para]
    #prepare the column named by ind, model parameter,standard deviation of parameter, RSS and model name
    col_name = ind + para[1:] + para_sd[1:] + ["RSS", "model"]
    
    #initial the np array to store the fitted parameter
    para_fit_all = np.empty(len(col_name))
    
    #use for-loop to take data of each index
    for i in ind_list[0]:
        for j in ind_list[1]:
            #fit model
            ppot, pcov = curve_fit(model,
                                   df_g.loc[i].loc[j][x],
                                   df_g.loc[i].loc[j][y],
                                   bounds = ((0, 
                                              0, 
                                              df_g.loc[i].loc[j][y][-1] - 0.00001, #set logNmax as the last data in y coloumn
                                              0),
                                             [np.inf,
                                              np.inf,
                                              df_g.loc[i].loc[j][y][-1],
                                              np.inf]))
            
            #calculate RSS
            RSS = np.sum(np.square(model(df_g.loc[i].loc[j][x],*ppot) - 
                             df_g.loc[i].loc[j][y]))
            
            #calculate standard deviation errors
            perr = np.sqrt(np.diag(pcov))
            
            #save fitted parameter to para_fit_all
            para_fit_list = [i,j] + list(ppot) + list(perr) + [RSS, str(model)]
            para_fit = np.array(para_fit_list)
            para_fit_all = np.vstack((para_fit_all, para_fit))
            
    #return the DataFrame with fitted parameter
    return pd.DataFrame(para_fit_all[1:], columns = col_name)

#----------model fitting
# open the data file
Data = pd.read_csv(file_name)

#fit data with select model, here is a example for Gompertz model
fit_parameter = growth_fit(Gompertz,Data)

# save the fitted model parameter
fit_parameter.to_csv("Fitted_parameter.csv", index = False)

